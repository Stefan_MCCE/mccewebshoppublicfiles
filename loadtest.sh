#!/bin/sh

mkdir -p results
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_10vus_300s.js 
k6 run --vus 10 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results/result_10vus_300s.json --summary-export=results/summary_10vus_300s.json load.js 