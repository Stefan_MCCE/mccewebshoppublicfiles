#!/bin/sh

mkdir -p results-onprem
k6 run --vus 5 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_5vus_300s.json --summary-export=results-onprem/summary_5vus_300s.json load.js 
sleep 1m
k6 run --vus 8 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_8vus_300s.json --summary-export=results-onprem/summary_8vus_300s.json load.js 
sleep 1m
k6 run --vus 12 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_12vus_300s.json --summary-export=results-onprem/summary_12vus_300s.json load.js 
sleep 1m
k6 run --vus 15 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_15vus_300s.json --summary-export=results-onprem/summary_15vus_300s.json load.js 
sleep 1m
k6 run --vus 25 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_25vus_300s.json --summary-export=results-onprem/summary_25vus_300s.json load.js 
sleep 1m
k6 run --vus 40 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_40vus_300s.json --summary-export=results-onprem/summary_40vus_300s.json load.js 
sleep 1m
k6 run --vus 50 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_50vus_300s.json --summary-export=results-onprem/summary_50vus_300s.json load.js 
sleep 1m
k6 run --vus 80 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_80vus_300s.json --summary-export=results-onprem/summary_80vus_300s.json load.js 
sleep 1m
k6 run --vus 120 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_120vus_300s.json --summary-export=results-onprem/summary_120vus_300s.json load.js 
sleep 1m
k6 run --vus 160 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-onprem/result_160vus_300s.json --summary-export=results-onprem/summary_160vus_300s.json load.js 