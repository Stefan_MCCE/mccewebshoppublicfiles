import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
    { duration: '3m', target: 50 },
    { duration: '5m', target: 50 }
  ],
};


export default function () {
  const response = http.get(`http://${__ENV.HOSTNAMEBACKEND}/api/product`, {
    tags: { name: 'Fetch all products' },
  });
  check(response, {
    'status was 200': r => r.status == 200
  });

  var productId = [2, 3, 16, 24, 55, 132, 317];
  var productPictureId = [
    "242ebd3522bc66b1ca1caf60c2484a50c55b7d06", 
    "9ccdb7d193854ee5ff6d118e3281f68349767cb1",
    "ba989a7814913a6f91f921bb4eb5e4fd7b6bfb5e",
    "69b82a61d361632e3573afe8ce7c8f24cca8ff06",
    "e105aba3278d723abcbb092c3eab8240347f19eb",
    "fcfc4e846620f94372d203e6ed1061a278566295",
    "539ef8e7ac2b50bd0b002789e8ca09a17d26f99c"
  ]

  for (var id = 0; id < 7; id++) {
    const responseProduct = http.get(`http://${__ENV.HOSTNAMEBACKEND}/api/product/${productId[id]}`, {
      tags: { name: `Fetch ProductDescription ${productId[id]}` },
    });
    check(responseProduct, {
      'status was 200': r => r.status == 200
    });
    const responsePicture = http.get(`http://${__ENV.HOSTNAMEIMAGE}/${productPictureId[id]}.jpg`, {
      tags: { name: `Fetch ProductImage ${productPictureId[id]}` },
    });
    check(responsePicture, {
      'status was 200': r => r.status == 200
    });
    sleep(1);
  }
};