# Webshop Load-Test

This project provides files to benchmark the webshop application.
To do so just call 
```shell
sh loadtest.sh 10.100.248.138:8080 10.100.58.13:81
```

This script has to be called with two parameter. 
The first one is the ip with port to the backend, the secound one is the ip with port from the imageserver.