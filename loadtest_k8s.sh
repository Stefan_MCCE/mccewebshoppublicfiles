#!/bin/sh

mkdir -p results-k8s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_5vus_300s.js
k6 run --vus 5 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_5vus_300s.json --summary-export=results-k8s/summary_5vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_8vus_300s.js
k6 run --vus 8 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_8vus_300s.json --summary-export=results-k8s/summary_8vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_12vus_300s.js
k6 run --vus 12 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_12vus_300s.json --summary-export=results-k8s/summary_12vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_15vus_300s.js
k6 run --vus 15 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_15vus_300s.json --summary-export=results-k8s/summary_15vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_25vus_300s.js
k6 run --vus 25 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_25vus_300s.json --summary-export=results-k8s/summary_25vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_40vus_300s.js
k6 run --vus 40 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_40vus_300s.json --summary-export=results-k8s/summary_40vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_50vus_300s.js
k6 run --vus 50 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_50vus_300s.json --summary-export=results-k8s/summary_50vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_80vus_300s.js
k6 run --vus 80 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_80vus_300s.json --summary-export=results-k8s/summary_80vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_120vus_300s.js
k6 run --vus 120 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_120vus_300s.json --summary-export=results-k8s/summary_120vus_300s.json load.js 
sleep 10s
k6 run --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 warmup_160vus_300s.js
k6 run --vus 160 --duration 300s --env HOSTNAMEBACKEND=$1 --env HOSTNAMEIMAGE=$2 --out json=results-k8s/result_160vus_300s.json --summary-export=results-k8s/summary_160vus_300s.json load.js 